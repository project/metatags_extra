<?php

/**
 * @file
 * Implement batch operations and Drupal actions.
 */

/**
 * Implements hook_node_operations().
 */
function metatags_extra_operations_node_operations() {
  $operations = array(
    'delete_metatags' => array(
      'label' => t('Delete meta tags'),
      'callback' => 'metatags_extra_operations_mass_update',
      'callback arguments' => array('type' => NODEWORDS_TYPE_NODE, 'operation' => 'delete'),
    ),
  );

  return $operations;
}

/**
 * Implements hook_user_operations().
 */
function metatags_extra_operations_user_operations() {
  $operations = array(
    'delete_metatags' => array(
      'label' => t('Delete meta tags'),
      'callback' => 'metatags_extra_operations_mass_update',
      'callback arguments' => array('type' => NODEWORDS_TYPE_USER, 'operation' => 'delete'),
    ),
  );

  return $operations;
}

/**
 * Delete the nodes meta tags.
 *
 * @param $ids
 *  An array of IDs.
 * @param $type
 *  The type of the object associated with the IDs (NODEWORDS_TYPE_NODE, NODEWORDS_TYPE_USER,
 *  NODEWORDS_TYPE_PAGER, NODEWORDS_TYPE_PAGE, ...).
 * @param $operation
 *  The operation to execute (currently implemented: delete).
 */
function metatags_extra_operations_mass_update($ids, $type, $operation = 'delete') {
  if ($operation == 'delete') {
    if (count($ids) <= 10) {
      db_query("DELETE FROM {nodewords} WHERE id IN (" . db_placeholders($ids, 'int') . ") AND type = %d",
        array_merge($ids, array($type))
      );

      if ($type == NODEWORDS_TYPE_PAGE) {
        foreach ($ids as $id) {
          module_invoke_all('metatags_delete',
            array('type' => NODEWORDS_TYPE_PAGE, 'id' => $id)
          );
        }
      }

      drupal_set_message(t('The update has been performed.'));
    }
    else {
      $batch = array(
        'operations' => array(
          array('_metatags_extra_operations_delete_batch_process', array($ids, $type))
        ),
        'finished' => '_metatags_extra_operations_update_batch_finished',
        'title' => t('Processing'),
        'progress_message' => '',
        'error_message' => t('The update has encountered an error.'),
        'file' => drupal_get_path('module', 'metatags_extra_operations') .'/metatags_extra_operations.inc',
      );
      batch_set($batch);
    }
  }
}

function _metatags_extra_operations_delete_batch_process($ids, $type, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($ids);
    $context['sandbox']['ids'] = $ids;
  }

  $count = min(5, count($context['sandbox']['ids']));

  if (!isset($context['results']['count'])) {
    $context['results']['count'] = 0;
  }

  for ($i = 1; $i <= $count; $i++) {
    $id = array_shift($context['sandbox']['ids']);

    db_query("DELETE FROM {nodewords} WHERE type = %d AND id = %d", $type, $id);

    module_invoke_all('metatags_delete', array('type' => $type, 'id' => $id));

    $context['results']['count']++;
    $context['sandbox']['progress']++;
  }

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

function _metatags_extra_operations_update_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('The update has been performed.'));
  }
  else {
    drupal_set_message(t('An error occurred and processing did not complete.'), 'error');

    $message = format_plural(
      $results['count'],
      '1 item successfully processed.',
      '@count items successfully processed.'
    );

    drupal_set_message($message);
  }
}
