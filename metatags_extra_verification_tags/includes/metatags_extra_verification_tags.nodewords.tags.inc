<?php

/**
 * @file
 * Nodewords support file.
 */

/**
 * Set the form fields used to implement the options for the meta tag.
 */
function metatags_extra_verification_tags_alexa_verification_code_form(&$form, $content, $options) {
  $form['alexa_verification_code'] = array(
    '#tree' => TRUE,
  );

  $form['alexa_verification_code']['value'] = array(
    '#type' => 'textarea',
    '#title' => t('Alexa verification code'),
    '#description' => t('<a href="@alexa-url">Alexa</a> will ask you to add a meta tag in order to verify you have write access to the pages of the web site you registered with their services. You can obtain a verification code <a href="@alexa-claim-your-site">claiming your site</a>', array('@alexa-url' => 'http://www.alexa.com', '@alexa-claim-your-site' => 'http://www.alexa.com/siteowners/claim')) . $options['description'],
    '#default_value' => empty($content['value']) ? '' : $content['value'],
    '#wysiwyg' => FALSE,
  );
}

/**
 * Set the meta tag content.
 */
function metatags_extra_verification_tags_alexa_verification_code_prepare(&$tags, $content, $options) {
  if (!empty($content['value'])) {
    $tags['alexa_verification_code:alexaVerifyID'] = $content['value'];
  }
}

/**
 * Set the form fields used to implement the options for the meta tag.
 */
function metatags_extra_verification_tags_bing_webmaster_center_form(&$form, $content, $options) {
  $form['bing_webmaster_center'] = array(
    '#tree' => TRUE,
  );

  $form['bing_webmaster_center']['value'] = array(
    '#type' => 'textarea',
    '#title' => t('Bing Webmaster Center verification code'),
    '#description' => t('<a href="@bing-url">Bing Webmaster Center</a> will ask you to add a meta tag in order to verify you have write access to the pages of the web site you registered with their services. Enter one code per line.', array('@bing-url' => 'http://www.bing.com/webmaster')) . $options['description'],
    '#default_value' => empty($content['value']) ? '' : $content['value'],
    '#wysiwyg' => FALSE,
  );
}

/**
 * Set the meta tag content.
 */
function metatags_extra_verification_tags_bing_webmaster_center_prepare(&$tags, $content, $options) {
  if (!empty($content['value'])) {
    $value = preg_split('/(\r\n?|\n)/', $content['value'], -1, PREG_SPLIT_NO_EMPTY);
    $tags['bing_webmaster_center:msvalidate.01'] = $value;
  }
}

/**
 * Set the form fields used to implement the options for the meta tag.
 */
function metatags_extra_verification_tags_google_webmaster_tools_form(&$form, $content, $options) {
  $form['google_webmaster_tools'] = array(
    '#tree' => TRUE,
  );

  $form['google_webmaster_tools']['value'] = array(
    '#type' => 'textarea',
    '#title' => t('Google Webmaster Tools verification code'),
    '#description' => t('<a href="@google-url">Google Webmaster Tools</a> will ask you to add a meta tag in your web site to provide you with an easy way to make your site more <a href="http://google.com">Google</a>-friendly. Enter one code per line.', array('@google-url' => 'http://www.google.com/webmasters/tools')) . $options['description'],
    '#default_value' => empty($content['value']) ? '' : $content['value'],
    '#wysiwyg' => FALSE,
  );
}

/**
 * Set the meta tag content.
 */
function metatags_extra_verification_tags_google_webmaster_tools_prepare(&$tags, $content, $options) {
  if (!empty($content['value'])) {
    $value = preg_split('/(\r\n?|\n)/', $content['value'], -1, PREG_SPLIT_NO_EMPTY);
    $tags['google_webmaster_tools:google-site-verification'] = $value;
  }
}

/**
 * Set the form fields used to implement the options for the meta tag.
 */
function metatags_extra_verification_tags_yahoo_site_explorer_form(&$form, $content, $options) {
  $form['yahoo_site_explorer'] = array(
    '#tree' => TRUE,
  );

  $form['yahoo_site_explorer']['value'] = array(
    '#type' => 'textarea',
    '#title' => t('Yahoo! Site Explorer'),
    '#description' => t('<a href="@yahoo-url">Yahoo! Site Explorer</a> will ask you to add a meta tag in your web site to allow you to explore all the web pages indexed by <a href="http://search.yahoo.com">Yahoo! Search</a>. Enter one code per line.', array('@yahoo-url' => 'https://siteexplorer.search.yahoo.com/')) . $options['description'],
    '#default_value' => empty($content['value']) ? '' : $content['value'],
    '#wysiwyg' => FALSE,
  );
}

/**
 * Set the meta tag content.
 */
function metatags_extra_verification_tags_yahoo_site_explorer_prepare(&$tags, $content, $options) {
  if (!empty($content['value'])) {
    $value = preg_split('/(\r\n?|\n)/', $content['value'], -1, PREG_SPLIT_NO_EMPTY);
    $tags['yahoo_site_explorer:y_key'] = $value;
  }
}
