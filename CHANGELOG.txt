# All the dates reported here are referred to UTC.

2010-05-31
----------
#813606 by kiam: Support for Alexa verification tag.

2010-05-28
----------
#753702 by ayalon, kiam: Some meta tags are not output.

2010-05-19
----------
#775446 by kiam: Use again tokens for the meta tag DC.DATE.

2010-05-05
----------
#761392 by kiam: Create a module to implement permissions associated with meta tags.

2010-04-19
----------
#775446 by kiam: Meta tag DC.DATE doesn't verify the entered value.

2010-04-18
----------
#774350 by kiam: Fatal error: Call to undefined function nodewords_check_version_api().

2010-04-14
----------
#653824 by kiam: Support for multiple verification tags, and short URLs.

2010-04-11
----------
#767764 by kiam: Token for the front page URL should return the base URL.

2010-04-10
----------
#767190 by kiam: Token [metatags-url-alias] does not return the path alias.
