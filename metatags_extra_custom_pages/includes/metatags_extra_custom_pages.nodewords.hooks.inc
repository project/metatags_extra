<?php

/**
 * @file
 * Nodewords support file.
 */

/**
 * Implements hook_metatags_delete().
 */
function metatags_extra_custom_pages_metatags_delete($options) {
  if ($options['type'] == NODEWORDS_TYPE_PAGE) {
    db_query("DELETE FROM {metatags_extra_custom_pages} WHERE pid = '%s'", $options['id']);
  }
}

/**
 * Implements hook_metatags_type().
 */
function metatags_extra_custom_pages_metatags_type(&$result, $arg) {
  foreach (_metatags_extra_custom_pages_load_data() as $page) {
    $path = $page->path;

    if (drupal_match_path($_GET['q'], $path)) {
      $result['type'] = NODEWORDS_TYPE_PAGE;
      $result['id'] = $page->pid;
      break;
    }

    $bool = (
      ($alias = drupal_get_path_alias($_GET['q'])) != $_GET['q'] &&
      drupal_match_path($alias, $path)
    );
    if ($bool) {
      $result['type'] = NODEWORDS_TYPE_PAGE;
      $result['id'] = $page->pid;
      break;
    }
  }
}
